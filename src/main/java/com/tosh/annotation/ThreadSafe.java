package com.tosh.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * User: arsentyev
 * Date: 27.07.12
 */
@Retention(RetentionPolicy.CLASS)
public @interface ThreadSafe {}