package com.tosh.sql;

/**
 * User: antosha
 * Date: 23.12.13
 */
public class SqlBuilder {
    private StringBuilder query = null;

    public SqlBuilder() {
        query = new StringBuilder();
    }

    public SqlBuilder orderBy(String fieldName, OrderBy orderBy) {
        query.append(" order by ").append(fieldName).append(" ").append(orderBy.name().toLowerCase());
        return this;
    }

    public SqlBuilder select(String... fields) {
        query.append("select ");
        int i = 1;
        for(String field : fields) {
            query.append(field);

            if(i++ != fields.length) {
                query.append(", ");
            }
        }
        return this;
    }

    public SqlBuilder from(String tableName) {
        query.append(" from ").append(tableName);
        return this;
    }

    public SqlBuilder from(String tableName, String alias) {
        query.append(" from ").append(tableName).append(" as ").append(alias);
        return this;
    }

    public SqlBuilder where(String whereField, Operator operator, Object value) {
        query.append(" where ")
             .append(whereField)
             .append(" ").append(operator.getText())
             .append(" ").append(escape(value));
        return this;
    }

    public SqlBuilder and(String whereField, Operator operator, Object value) {
        query.append(" and ")
                .append(whereField)
                .append(" ").append(operator.getText())
                .append(" ").append(escape(value));
        return this;
    }

    public SqlBuilder or(String whereField, Operator operator, Object value) {
        query.append(" or ")
                .append(whereField)
                .append(" ").append(operator.getText())
                .append(" ").append(escape(value));
        return this;
    }

    private String escape(Object value) {
        String result = null;

        if(value == null) {
            result = "NULL";
        } else
        if(value instanceof String) {
            result = "\"" + value + "\"";
        } else {
            result = "" + value;
        }

        return result;
    }

    // specific functions

    public SqlBuilder limit(int limit) {
        query.append(" limit ").append(limit);
        return this;
    }

    public String toQuery() {
        return query.toString();
    }

    //some test
    public static void main(String... args) {
        SqlBuilder builder = new SqlBuilder();
        builder
        .select("id", "name").from("users")
        .where("name", Operator.LIKE, "vasya%")
        .and("login", Operator.NOT_EQUAL, "moron")
        .orderBy("name", OrderBy.ASC)
        .limit(4);

        System.out.println(builder.toQuery());
    }
}