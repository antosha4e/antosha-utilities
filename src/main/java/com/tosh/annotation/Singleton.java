package com.tosh.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: arsentyev
 * Date: 30.07.12
 */
@Target(value= ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Singleton {}