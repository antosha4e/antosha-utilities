package test;

import com.tosh.util.ParseUtils;
import junit.framework.TestCase;

import java.util.Calendar;

import static com.tosh.util.ParseUtils.*;

/**
 * User: arsentyev
 * Date: 24.10.12
 */
public class ParseUtilsTest extends TestCase {
    public void testTryParseLong() throws Exception {
        assertEquals(1l, tryParseLong("1"));
        assertEquals(-1l, tryParseLong("ff"));
        assertEquals(2l, tryParseLong("ff", 2l));
        assertNotNull(tryParseLongObj("1"));
        assertNull(tryParseLongObj("a2"));
    }

    public void testTryParseDouble() throws Exception {
        assertEquals(1.0d, tryParseDouble("1"));
        assertEquals(-1d, tryParseDouble("ff"));
        assertEquals(2d, tryParseDouble("ff", 2d));
        assertNotNull(tryParseDoubleObj("1"));
        assertNull(tryParseDoubleObj("a2"));

        ParseUtils.setDefaultDouble(Double.NaN);
        assertEquals(Double.NaN, tryParseDouble("ff"));
    }

    public void testTryParseFloat() throws Exception {
        assertEquals(1f, tryParseFloat("1"));
        assertEquals(-1f, tryParseFloat("ff"));
        assertEquals(2f, tryParseFloat("ff", 2l));
        assertNotNull(tryParseFloatObj("1"));
        assertNull(tryParseFloatObj("a2"));
    }

    public void testTryParseInt() throws Exception {
        assertSame(1, tryParseInt("1"));
        assertSame(-1, tryParseInt("ff"));
        assertSame(2, tryParseInt("ff", 2));
        assertNotNull(tryParseIntObj("1"));
        assertNull(tryParseIntObj("a2"));
    }

    public void testTryParseDate() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(2001, 1, 11, 12, 22, 30);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime().getTime(), tryParseDate("2001-02-11 12:22:30").getTime());
        assertEquals(cal.getTime().getTime(), tryParseDate("ff", cal.getTime()).getTime());
        assertNull(tryParseDate("a2"));
    }
}