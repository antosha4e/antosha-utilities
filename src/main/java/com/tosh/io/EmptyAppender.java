package com.tosh.io;

import java.io.IOException;

/**
 * User: arsentyev
 * Date: 30.01.13
 */
public class EmptyAppender implements Appendable {
    public Appendable append(CharSequence csq) throws IOException {
        return this;
    }

    public Appendable append(CharSequence csq, int start, int end) throws IOException {
        return this;
    }

    public Appendable append(char c) throws IOException {
        return this;
    }
}