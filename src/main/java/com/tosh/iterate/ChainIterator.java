package com.tosh.iterate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Iterates via several iterators in the chain
 *
 * User: arsentyev
 * Date: 26.12.11
 */
public class ChainIterator<T> implements Iterator<T> {
    private List<Iterator<? extends T>> _iterators = new ArrayList<Iterator<? extends T>>();
    private int _index = 0;
    private T _next;
    private boolean _advanced;

    public ChainIterator(Iterator<? extends T> iterator) {
        if(iterator != null){
            _iterators.add(iterator);
        }
    }

    public ChainIterator() { /*no args constructor*/ }

    public void addIterator(Iterator<? extends T> iterator) {
        if(iterator != null){
            _iterators.add(iterator);
        }
    }

    public boolean hasNext() {
        return _advanced || advance();
    }

    public T next() {
        if (!_advanced) {
            if (!advance()) {
                throw new NoSuchElementException();
            }
        }
        _advanced = false;
        return _next;
    }

    public void remove() {
        throw new IllegalStateException("remove() cannot be called");
    }

    private boolean advance() {
        if (_iterators.size() <= _index) {
            return false;
        }
        Iterator<? extends T> iterator = _iterators.get(_index);
        if(iterator == null) {
            return false;
        }

        if(iterator.hasNext()) {
            _next = iterator.next();
            _advanced = true;
            return true;
        } else {
            _index++;
            return advance();
        }
    }
}