package test;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Collections;

import static com.tosh.util.ToshUtils.*;

/**
 * User: arsentyev
 * Date: 25.10.12
 */
public class ToshUtilsTest extends TestCase {
    public void testIsNOE() {
        assertTrue(isNOE(""));
        assertTrue(isNOE((String)null));
        assertFalse(isNOE("a"));


        assertTrue(isNOE(Collections.emptyMap()));
        assertTrue(isNOE(Collections.emptyList()));
        assertTrue(isNOE(Collections.emptySet()));
        assertTrue(isNOE(new Integer[]{}));
        assertFalse(isNOE(Arrays.asList(new String[]{"1"})));
        assertFalse(isNOE(new String[]{"1"}));
    }

    public void testIsNOEO() {
        assertTrue(isNOEO("", "1"));
        assertTrue(isNOEO("2", null));
        assertFalse(isNOEO("2", "1"));
    }

    public void testIsNOEA() {
        assertTrue(isNOEA("", null));
        assertTrue(isNOEA(null, null));
        assertFalse(isNOEA("2", null));
        assertFalse(isNOEA("2", "12"));
    }

    public void testIfNOE() {
        assertEquals("1", ifNOE("", "1"));
    }

    public void testIsLowerCase() {
        assertTrue(isLowerCase("lollolo"));
        assertFalse(isLowerCase("lolAolo"));
    }

    public void testIsUpperCase() {
        assertTrue(isUpperCase("AADAF1"));
        assertFalse(isUpperCase("AADdDS"));
    }

    public void testCapitalize() {
        assertEquals("Anton", capitalize("anton"));
        assertEquals("Anton", capitalize("anTon"));
        assertNotSame("omg", capitalize("omg"));
    }

    public void testIsCapitalized() {
        assertTrue(isCapitalized("Anton"));
        assertTrue(isCapitalized("Anton123"));
        assertFalse(isCapitalized("anton"));
        assertFalse(isCapitalized("aNton"));
    }

    public void testSplit() {
        String strToSplit1 = "abc,bcd,omg,zzz,aaa,ooo,lll,128372183,123213,123,321,3,12,3,213,123";
        String strToSplit2 = "abc|bcd|omg|zzz|aaa||ooo|lll|";
        char c1 = ',', c2 = '|';

        assertEquals(Arrays.toString(split(strToSplit1, c1)), Arrays.toString(strToSplit1.split(String.valueOf(c1))));
        assertEquals(Arrays.toString(split(strToSplit2, c2)), Arrays.toString(strToSplit2.split("\\" + c2)));

        long t = System.currentTimeMillis(), n = 100000;

        for(int i = 0; i< n; i++) {
            Arrays.toString(split(strToSplit1, c1));
        }

        System.out.println(System.currentTimeMillis() - t);
        t = System.currentTimeMillis();

        for(int i = 0; i< n; i++) {
            Arrays.toString(strToSplit1.split(String.valueOf(c1)));
        }

        System.out.println(System.currentTimeMillis() - t);
    }

    public void testString() {
//        System.out.println(StringUtils.concat(null, "cd", "efg"));
//        System.out.println(StringUtils.repeat('a', 4));
//        System.out.println('|' + StringUtils.space(5) + '|');
//        System.out.println(StringUtils.replicate("omg_", 3));
    }
}