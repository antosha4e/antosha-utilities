package com.tosh.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.tosh.util.ToshUtils.isNOE;

/**
 * User: arsentyev
 * Date: 21.01.13
 */
final class SecurityUtils {

    /**
     * Get Hash of the passed String with the given algorithm
     * @param str String to hashing
     * @param alg hash algorithm
     * @return hash of the passed String
     */
    static String getHash(String str, String alg) {
        if(isNOE(str)) {
            return  str;
        }
        if(isNOE(alg)) {
            alg = "SHA-512";
        }
        byte byteData[] = getHashBytes(str, alg);
        if(byteData == null) {
            return str;
        }

        StringBuilder sb = new StringBuilder();
        for (byte aByteData1 : byteData) {
            sb.append(Integer.toString((aByteData1 & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    /**
     * Get Hash of the passed String with the given algorithm
     * @param str String to hashing
     * @param alg hash algorithm
     * @return hash of the passed String
     */
    static byte[] getHashBytes(String str, String alg) {
        if(isNOE(str)) {
            return  null;
        }
        if(isNOE(alg)) {
            alg = "SHA-512";
        }
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(alg);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        md.update(str.getBytes());

        return md.digest();
    }
}