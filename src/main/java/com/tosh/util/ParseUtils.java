package com.tosh.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.tosh.util.ToshUtils.isNOE;

/**
 * Banch of functions for parsing
 *
 * <br>
 * User: arsentyev
 * Date: 27.07.12
 */
public final class ParseUtils {
    /* Defaults values */
    private static int    defInt = -1;
    private static long   defLng = -1l;
    private static double defDbl = -1d;
    private static float  defFlt = -1f;
    private static String defFormat = "yyyy-MM-dd HH:mm:ss";

    private ParseUtils(){}

    public static void setDefaultInt(int defInt) {
        ParseUtils.defInt = defInt;
    }

    public static void setDefaultLong(long defLng) {
        ParseUtils.defLng = defLng;
    }

    public static void setDefaultDouble(double defDbl) {
        ParseUtils.defDbl = defDbl;
    }

    public static void setDefaultFloat(float defFlt) {
        ParseUtils.defFlt = defFlt;
    }

    public static void setDefaultFormat(String defFormat) {
        ParseUtils.defFormat = defFormat;
    }

    /**
     * Parsing Long from String representation
     * @param lStr String representing long
     * @return parsed Long, or NULL value if parsing failed
     */
    public static Long tryParseLongObj(String lStr) {
        if (isNOE(lStr)) {
            return null;
        }
        try {
            return Long.parseLong(lStr, 10);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Parsing Long from String representation
     * @param lStr String representing long
     * @param def default value, which returned if parsing fails
     * @return parsed long, or def value if parsing failed
     */
    public static long tryParseLong(String lStr, long def) {
        if (isNOE(lStr)) {
            return def;
        }
        try {
            return Long.parseLong(lStr, 10);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    /**
     * Parsing Long from String representation
     * @param lStr String representing long
     * @return parsed long
     */
    public static long tryParseLong(String lStr) {
        return tryParseLong(lStr, defLng);
    }

    /**
     * Parsing Double from String representation
     * @param dStr String representing double
     * @return parsed Double, or NULL value if parsing failed
     */
    public static Double tryParseDoubleObj(String dStr) {
        if (isNOE(dStr)) {
            return null;
        }
        try {
            return Double.parseDouble(dStr);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Parsing Double from String representation
     * @param dStr String representing double
     * @param def default value, which returned if parsing fails
     * @return parsed double, or def value if parsing failed
     */
    public static double tryParseDouble(String dStr, double def) {
        if (isNOE(dStr)) {
            return def;
        }
        try {
            return Double.parseDouble(dStr);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    /**
     * Parsing Double from String representation
     * @param dStr String representing double
     * @return parsed double
     */
    public static double tryParseDouble(String dStr) {
        return tryParseDouble(dStr, defDbl);
    }

    /**
     * Parsing Float from String representation
     * @param fStr String representing float
     * @return parsed Float, or NULL value if parsing failed
     */
    public static Float tryParseFloatObj(String fStr) {
        if (isNOE(fStr)) {
            return null;
        }
        try {
            return Float.parseFloat(fStr);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Parsing Float from String representation
     * @param fStr String representing float
     * @param def default value, which returned if parsing fails
     * @return parsed float, or def value if parsing failed
     */
    public static float tryParseFloat(String fStr, float def) {
        if (isNOE(fStr)) {
            return def;
        }
        try {
            return Float.parseFloat(fStr);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    /**
     * Parsing Float from String representation
     * @param fStr String representing float
     * @return parsed float
     */
    public static float tryParseFloat(String fStr) {
        return tryParseFloat(fStr, defFlt);
    }

    /**
     * Parsing Integer from String representation
     * @param iStr String representing int
     * @return parsed Integer, or NULL value if parsing failed
     */
    public static Integer tryParseIntObj(String iStr) {
        if (isNOE(iStr)) {
            return null;
        }
        try {
            return Integer.parseInt(iStr, 10);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Parsing Integer from String representation
     * @param iStr String representing int
     * @param def default value, which returned if parsing fails
     * @return parsed int, or def value if parsing failed
     */
    public static int tryParseInt(String iStr, int def) {
        if (isNOE(iStr)) {
            return def;
        }
        try {
            return Integer.parseInt(iStr, 10);
        } catch (NumberFormatException e) {
            return def;
        }
    }

    /**
     * Parsing Integer from String representation
     * @param iStr String representing int
     * @return parsed int
     */
    public static int tryParseInt(String iStr) {
        return tryParseInt(iStr, defInt);
    }

    /**
     * Parsing Date from String representation with given format
     * @param date String representing date in specified format
     * @param def default value, which returned if parsing fails
     * @param format format used to parse date
     * @return parsed date, or def value if parsing failed
     */
    public static Date tryParseDate(String date, Date def, String format) {
        if (isNOE(date)) {
            return def;
        }
        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (ParseException e) {
            return def;
        }
    }

    /**
     * Parsing Date from String representation
     * @param date String representing date in "yyyy-MM-dd'T'HH:mm:ss.SSS" format
     * @param def default value, which returned if parsing fails
     * @return parsed date, or def value if parsing failed
     */
    public static Date tryParseDate(String date, Date def) {
        return tryParseDate(date, def, defFormat);
    }

    /**
     * Parsing Date from String representation
     * @param date String representing date in "yyyy-MM-dd'T'HH:mm:ss.SSS" format
     * @return parsed date
     */
    public static Date tryParseDate(String date) {
        return tryParseDate(date, null, defFormat);
    }

    /**
     * Parsing Date from String representation with given format
     * @param date String representing date in specified format
     * @param format format used to parse date
     * @return parsed date, or def value if parsing failed
     */
    public static Date tryParseDate(String date, String format) {
        return tryParseDate(date, null, format);
    }
}