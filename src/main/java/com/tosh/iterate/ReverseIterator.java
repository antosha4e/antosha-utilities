package com.tosh.iterate;

import java.util.Iterator;
import java.util.ListIterator;

/**
 * Iterate list iterator in reverse order
 *
 * User: arsentyev
 * Date: 27.01.12
 */
public class ReverseIterator<E> implements Iterator<E> {
    private ListIterator<E> _listIterator = null;

    public ReverseIterator(ListIterator<E> listIterator) {
        this._listIterator = listIterator;
    }

    public boolean hasNext() {
        if(_listIterator == null) {
            return false;
        }
        return _listIterator.hasPrevious();
    }

    public E next() {
        if(_listIterator == null) {
            return null;
        }
        return _listIterator.previous();
    }

    public void remove() {
        if(_listIterator != null) {
            _listIterator.remove();
        }
    }
}