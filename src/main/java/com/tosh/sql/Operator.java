package com.tosh.sql;

/**
 * User: antosha
 * Date: 24.12.13
 */
public enum Operator {
    LIKE("LIKE"), EQUAL("="), NOT_EQUAL("<>");

    Operator(String text) {
        this.text = text;
    }

    private String text;

    public String getText() {
        return this.text;
    }
}