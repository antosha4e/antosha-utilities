package com.tosh.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * User: arsentyev
 * Date: 26.07.12
 */
public final class Serializer {
    private final String pathName;
    private static final Serializer _instance = new Serializer();

    private Serializer(){
        this.pathName = null;
    }

    public Serializer(String pathName) {
        this.pathName = pathName;
        ToshUtils.createDir(pathName);
    }

    public Object load() throws FileNotFoundException, ClassNotFoundException {
        return loadFile(this.pathName);
    }

    public static Object load(String pathName) throws FileNotFoundException, ClassNotFoundException {
        return _instance.loadFile(pathName);
    }

    private Object loadFile(String pathName) throws FileNotFoundException, ClassNotFoundException {
        File file = new File(pathName);
        Object obj = null;
        try {
            if(file.canRead()){
                obj = _load(file);
            } else {
                throw new FileNotFoundException("Can't find readable file");
            }
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            obj = loadBackup(file.getAbsolutePath());
        }

        return obj;
    }

    private Object loadBackup(String pathName) {
        Object obj = null;
        try {
            File file = new File(pathName + "_back");
            if(file.canRead()){
                obj = _load(file);
            }
        }
        catch (IOException e) {/**/}
        catch (ClassNotFoundException e) {/**/}

        return obj;
    }

    private Object _load(File file) throws IOException, ClassNotFoundException {
        Object obj = null;
        ObjectInputStream ois = null;
        try {
            //todo mb sync on the pathName
            ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
            obj = ois.readObject();
//            if(obj instanceof Collection) {//check data consistency
//                ((Collection) obj).size();
//            }
        } finally {
            if(ois != null) {
                ois.close();
            }
        }
        return obj;
    }

    public void serialize(Object obj) throws IOException {
        _serialize(obj, this.pathName);
    }

    public static void serialize(Object obj, String pathName) throws IOException {
        ToshUtils.createDir(pathName);
        _instance._serialize(obj, pathName);
    }

    private void _serialize(Object obj, String pathName) throws IOException {
        ObjectOutputStream oos = null;
        if (obj != null) {
            serializeBackup(pathName);
            try {
                oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(pathName)));
                oos.writeObject(obj);
                oos.flush();
            } finally {
                if (oos != null) {
                    oos.close();
                }
            }
            clearBackup(pathName);
        }
    }

    private void serializeBackup(String pathName) {
        File file = new File(pathName);
        if(file.canRead()) {
            file.renameTo(new File(file.getAbsolutePath() + "_back"));
        }
    }

    private void clearBackup(String pathName) {
        File file = new File(pathName);
        File file1 = new File(file.getAbsolutePath() + "_back");
        if(file1.canRead()) {
            file1.delete();
        }
    }
}