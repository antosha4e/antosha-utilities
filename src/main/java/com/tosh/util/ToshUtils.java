package com.tosh.util;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

/**
 * User: arsentyev
 * Date: 27.07.12
 */
public final class ToshUtils {
    private ToshUtils() {}

    /* Logical methods */

    /**
     * Is String <b>null</b> or <b>empty</b>
     * @param str String to check
     * @return <b>true</b> - if passed String is NOE, otherwise <b>false</b>
     */
    public static boolean isNOE(String str) {
        return str == null || str.isEmpty();
    }

    /**
     * Is Collection <b>null</b> or <b>empty</b>
     * @param col Collection to check
     * @return <b>true</b> - if passed Collection is NOE, otherwise <b>false</b>
     */
    public static boolean isNOE(Collection col) {
        return col == null || col.isEmpty();
    }

    /**
     * Is Map <b>null</b> or <b>empty</b>
     * @param map Map to check
     * @return <b>true</b> - if passed Map is NOE, otherwise <b>false</b>
     */
    public static boolean isNOE(Map map) {
        return map == null || map.isEmpty();
    }

    /**
     * Is Array <b>null</b> or <b>empty</b>
     * @param t Array to check
     * @param <T> Type of passed array
     * @return <b>true</b> - if passed Array is NOE, otherwise <b>false</b>
     */
    public static <T> boolean isNOE(T[] t) {
        return t == null || t.length == 0;
    }

    /**
     * Is <b>null</b> or <b>empty</b> at least one of the passed params
     * @param strs Strings to check
     * @return <b>true</b> - if at least one of passed params is NOE, otherwise <b>false</b>
     */
    public static boolean isNOEO(String... strs) {
        if(isNOE(strs)) {
            return true;
        }
        for(String s: strs) {
            if(isNOE(s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Is <b>null</b> or <b>empty</b> all of the passed params
     * @param strs Strings to check
     * @return <b>true</b> - if all of passed params is NOE, otherwise <b>false</b>
     */
    public static boolean isNOEA(String... strs) {
        if(isNOE(strs)) {
            return true;
        }
        boolean n = true;
        for(String s: strs) {
            n = n & isNOE(s);
            if(!n) {
                return false;
            }
        }
        return n;
    }

    /**
     * Get String value if its not NOE
     * @param value value to check
     * @param defaultValue default value to return
     * @return value if its not NOE, otherwise return defaultValue
     */
    public static String ifNOE(String value, String defaultValue) { //mb can be deleted
        return isNOE(value) ? defaultValue : value;
    }

    /**
     * Get value if its not <b>null</b>
     * @param value value to check
     * @param defaultValue default value to return
     * @param <T> type of value
     * @return value if its not <b>null</b>, otherwise return defaultValue
     */
    public static <T> T ifNull(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    /**
     * Get value if its not <b>null</b>
     * @param values values to check
     * @param <T> type of value
     * @return return first nonNull value
     */
    public static <T> T ifNull(T... values) {
        for(T val : values) {
            if(val != null) {
                return val;
            }
        }
        return null;
    }

    /**
     * Return <b>null</b> if objects are equals
     * @param value1 first value
     * @param value2 second value
     * @param <T> type of value
     * @return <b>null</b> if values are equals, otherwise return first value
     */
    public static <T> Object nullIf(T value1, T value2) {
        return value1 == value2 ? null : value1;
    }

    /* IO/Security methods */

    /**
     * Get Hash of the passed String with default algorithm 'SHA-512'
     * @param str String to hashing
     * @return hash of the passed String
     */
    public static String getHash(String str) {
        return getHash(str, "SHA-512");
    }

    /**
     * Get Hash of the passed String with the given algorithm
     * @param str String to hashing
     * @param alg hash algorithm
     * @return hash of the passed String
     */
    public static String getHash(String str, String alg) {
        return SecurityUtils.getHash(str, alg);
    }

    /**
     * Get Hash of the passed String with default algorithm 'SHA-512'
     * @param str String to hashing
     * @return hash of the passed String
     */
    public static byte[] getHashBytes(String str) {
        return getHashBytes(str, "SHA-512");
    }

    /**
     * Get Hash of the passed String with the given algorithm
     * @param str String to hashing
     * @param alg hash algorithm
     * @return hash of the passed String
     */
    public static byte[] getHashBytes(String str, String alg) {
        return SecurityUtils.getHashBytes(str, alg);
    }

    /**
     * Create new directory
     * @param dirName directory to create
     * @return <b>true</b> if dir was created, otherwise <b>false</b>
     */
    public static boolean createDir(String dirName) {
        return IOUtils.createDir(dirName);
    }

    /**
     * Reading InputStream to String
     * @param is Stream to read
     * @return String from Stream
     */
    public static String streamToString(InputStream is) {
        return IOUtils.streamToString(is);
    }

    /* String methods */

    /**
     * Converts passed Map&lt;String, String[]> to Map&lt;String, String>
     * @param rMap map to convert
     * @return converted Map&lt;String, String>
     */
    public static Map<String, String> toStringMap(Map<String, String[]> rMap) {
        return StringUtils.toStringMap(rMap);
    }

    /**
     * Check whenever String in lower case (English lang)
     * @param str String to check on lower case
     * @return <b>true</b> if passed String is in lower case, otherwise <b>false</b>
     */
    public static boolean isLowerCase(String str) {
        return StringUtils.isLowerCase(str);
    }

    /**
     * Check whenever String in upper case (English lang)
     * @param str String to check on upper case
     * @return <b>true</b> if passed String is in upper case, otherwise <b>false</b>
     */
    public static boolean isUpperCase(String str) {
        return StringUtils.isUpperCase(str);
    }

    /**
     * Is capitalized String (English lang)
     * @param s String to check
     * @return true if string is capitalized
     */
    public static boolean isCapitalized(String s) {
        return StringUtils.isCapitalized(s);
    }

    /**
     * Capitalize String (English lang)
     * @param s String to capitalize
     * @return capitalized String
     */
    public static String capitalize(String s) {
        return StringUtils.capitalize(s);
    }

    /**
     * ...
     * @param s string to split
     * @param c separator
     * @return array of splitted strings
     */
    public static String[] split(String s, char c) {
        return StringUtils.split(s, c);
    }

    /**
     * Reverse string
     * @param str string to be reversed
     * @return reversed string
     */
    public static String reverse(String str) {
        return StringUtils.reverse(str);
    }

    /**
     * Return string with certain number of spaces
     * @param size number of spaces
     * @return string with spaces
     */
    public static String space(int size) {
        return StringUtils.space(size);
    }

    /**
     * Repeat character certain times
     * @param c char to be repeated
     * @param size number of times
     * @return string of repeated chars
     */
    public static String repeat(char c, int size) {
        return StringUtils.repeat(c, size);
    }

    /**
     * Repeat string certain times
     * @param str char to be repeated
     * @param size number of times
     * @return string of repeated string
     */
    public static String repeat(String str, int size) {
        return StringUtils.repeat(str, size);
    }

    /**
     * Repeat string certain times
     * @param str string to be repeated
     * @param size number of times
     * @return result of repetitions
     */
    public static String replicate(String str, int size) {
        return StringUtils.replicate(str, size);
    }

    /**
     * Concatenate strings
     * @param strs banch of Strings to be concatenated
     * @return result of concatenation
     */
    public static String concat(String... strs) {
        return StringUtils.concat(strs);
    }

    /**
     * Trim the string
     * @param str string to trim
     * @return trimmed string
     */
    public static String trim(String str) {
        return StringUtils.trim(str);
    }

    public static String trimToSize(String str, int size) {
        return StringUtils.trimToSize(str, size);
    }

    public static String toUpper(String str) {
        return StringUtils.toUpper(str);
    }

    public static String toLower(String str) {
        return StringUtils.toLower(str);
    }

    public static String toHex(String str) {
        return StringUtils.toHex(str);
    }

    public static String fromHex(String str) {
        return StringUtils.fromHex(str);
    }
}