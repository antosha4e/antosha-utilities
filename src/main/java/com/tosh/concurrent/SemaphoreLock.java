package com.tosh.concurrent;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * User: arsentyev
 * Date: 16.08.12
 */
public class SemaphoreLock extends Semaphore {
    private final ReadLock readLock;
    private final WriteLock writeLock;

    private final int nThreads;

    public SemaphoreLock() {
        this(10);
    }

    public SemaphoreLock(int permits) {
        super(permits, true);
        nThreads = permits;
        readLock = new ReadLock();
        writeLock = new WriteLock();
    }

    public SemaphoreLock.WriteLock writeLock() {
        return writeLock;
    }

    public SemaphoreLock.ReadLock readLock() {
        return readLock;
    }

    public class ReadLock {
        public void lock() throws InterruptedException {
            acquire();
        }

        public void tryLock() {
            tryAcquire();//unfair
        }

        public void tryLock(long timeout, TimeUnit timeUnit) throws InterruptedException {
            tryAcquire(timeout, timeUnit);//unfair
        }

        public void unLock() {
            release();
        }
    }

    public class WriteLock {
        public void lock() throws InterruptedException {
            acquire(nThreads);
        }

        public void tryLock() {
            tryAcquire(nThreads);//unfair
        }

        public void tryLock(long timeout, TimeUnit timeUnit) throws InterruptedException {
            tryAcquire(nThreads, timeout, timeUnit);//unfair
        }

        public void unLock() {
            release(nThreads);
        }
    }
}