package com.tosh.util;

import java.io.File;
import java.io.InputStream;
import java.util.Scanner;

import static com.tosh.util.ToshUtils.isNOE;

/**
 * User: arsentyev
 * Date: 21.01.13
 */
final class IOUtils {
    /**
     * Create new directory
     * @param dirName directory to create
     * @return <b>true</b> if dir was created, otherwise <b>false</b>
     */
    static boolean createDir(String dirName) {
        if(!isNOE(dirName)) {
            File dir = new File(dirName);
            if(!dir.exists() || !dir.isDirectory()) {
                return dir.mkdir();
            }
        }
        return false;
    }

    /**
     * Reading InputStream to String
     * @param is Stream to read
     * @return String from Stream
     */
    static String streamToString(InputStream is) {
        try {return new Scanner(is, "UTF-8").useDelimiter("\\A").next();} catch (Exception e) {return null;}
    }
}