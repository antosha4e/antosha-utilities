package com.tosh.util;

import java.text.MessageFormat;

/**
 * Used to format some pattern with args.
 * Like MessageFormat.format()
 *
 * Not thread safe.
 *
 * User: arsentyev
 * Date: 04.12.12
 */
public class ToshMessages {
    private final StringBuilder message;
    private final String pattern;
    private int[] indexes;

    public ToshMessages(String pattern) {
        //check pattern
        this.pattern = pattern;
        this.message = new StringBuilder(pattern);
        this.buildIndexes();
    }

    private void buildIndexes() {
        int i = 0, len = 0, from = 0, cnt = 0;
        indexes = new int[5];
        while (true){
            i = message.indexOf("{" + len + "}", from);
            if(i != -1) {
                checkArray(len+1);
                indexes[len++] = i;
                from = i + 2;
            } else {
                int[] tmp = indexes;
                indexes = new int[len];
                System.arraycopy(tmp, 0, indexes, 0, len);
                break;
            }
        }
    }

    private void checkArray(int len) {
        if(indexes.length == len) {
            int[] tmp = indexes;
            indexes = new int[len+5];
            System.arraycopy(tmp, 0, indexes, 0, len);
        }
    }

    public String format(Object... arguments) {
        int i = 0, t = 3, offset = 0;
        String str = null;
        for(Object obj: arguments) {
            if(i == indexes.length) {
                break;
            }
            if(t < 4 && i > 9) {
                t = 4;
                if(t < 5 && t > 99) {
                    t = 5;
                }
            }
            str = String.valueOf(obj);
            message.replace(indexes[i] + offset, indexes[i] + t + offset, str);
            offset += str.length() - t;
            i++;
        }

        str = message.toString();
        reset();
        return str;
    }

    private void reset() {
        message.setLength(0);
        message.append(pattern);
    }

    public static String formatMessage(String pattern, Object... arguments) {
        return new ToshMessages(pattern).format(arguments);
    }

    public static void main(String... args) {
        test();
//        ToshMessages messages = new ToshMessages("Hello, {0}, {1}, {2}!");
//
//        System.out.println(messages.format("Anton", "Valera", "Dimon"));
//        System.out.println(messages.format("Antosha", "Valeron", "Dima"));
    }

    private static void test() {
        int n = 10000;
        long t = System.currentTimeMillis();

        ToshMessages messages = new ToshMessages("Hello, {0}, {1}, {2}!");

        for(int i = 0; i < n; i++) {
            messages.format("Anton", "Valera", "Dimon");
        }

        System.out.println(System.currentTimeMillis() - t);

        t = System.currentTimeMillis();

        for(int i = 0; i < n; i++) {
            MessageFormat.format("Hello, {0}, {1}, {2}!", "Anton", "Valera", "Dimon");
        }

        System.out.println(System.currentTimeMillis() - t);
    }
}