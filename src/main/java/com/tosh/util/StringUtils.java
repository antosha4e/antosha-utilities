package com.tosh.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.tosh.util.ToshUtils.isNOE;

/**
 * User: arsentyev
 * Date: 21.01.13
 */
final class StringUtils {

    /**
     * Reverse string
     * @param str string to be reversed
     * @return reversed string
     */
    static String reverse(String str) {
        if(isNOE(str)) {
            return str;
        }
        //mb replace with self-algo later
        return new StringBuilder(str).reverse().toString();
    }

    /**
     * Return string with certain number of spaces
     * @param size number of spaces
     * @return string with spaces
     */
    static String space(int size) {
        return repeat(' ', size);
    }

    /**
     * Repeat character certain times
     * @param c char to be repeated
     * @param size number of times
     * @return string of repeated chars
     */
    static String repeat(char c, int size) {
        char[] chars = new char[size];
        Arrays.fill(chars, c);
        return new String(chars);
    }

    /**
     * Repeat string certain times
     * @param str char to be repeated
     * @param size number of times
     * @return string of repeated string
     */
    static String repeat(String str, int size) {
        return new String(new char[size]).replace("\0", str);
    }

    /**
     * Repeat string certain times
     * @param str string to be repeated
     * @param size number of times
     * @return result of repetitions
     */
    static String replicate(String str, int size) {
        if(isNOE(str)) {
            return str;
        }
        String[] strs = new String[size];
        Arrays.fill(strs, str);
        return concat(strs);
    }

    /**
     * Concatenate strings
     * @param strs banch of Strings to be concatenated
     * @return result of concatenation
     */
    static String concat(String... strs) {
        //mb replace with stringBuilder
        char[] chars = new char[0];
        int len = 0;

        for(String s : strs) {
            if(s == null) {
                continue;
            }
            len = chars.length;

            chars = Arrays.copyOf(chars, len + s.length());
            System.arraycopy(s.toCharArray(), 0, chars, len, s.length());
        }

        return new String(chars);
    }

    /**
     * Converts passed Map&lt;String, String[]> to Map&lt;String, String>
     * @param rMap map to convert
     * @return converted Map&lt;String, String>
     */
    static Map<String, String> toStringMap(Map<String, String[]> rMap) {
        Map<String, String> map = new HashMap<String, String>();
        if(isNOE(rMap)){
            return map;
        }

        for (Map.Entry<String, String[]> entry : rMap.entrySet()) {
            String key = entry.getKey();
            String[] sArray = entry.getValue();

            if(sArray != null && sArray.length > 0 && !isNOE(key)) {
                map.put(key, sArray[0]);
            }
        }

        return map;
    }

    /**
     * Check whenever String in lower case (English lang)
     * @param str String to check on lower case
     * @return <b>true</b> if passed String is in lower case, otherwise <b>false</b>
     */
    static boolean isLowerCase(String str) {
        //97 - 122(a-z)  65 - 90(A-Z)
        //1072 - 1103(а-я)  1040 - 1071(А-Я)
        if(isNOE(str)) {
            return false;
        }
        char c;
        for(int i = 0; i < str.length(); i++) {
            c = str.charAt(i);
            if((c > 64 && c < 91) || (c > 1039 && c < 1072)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check whenever String in upper case (English lang)
     * @param str String to check on upper case
     * @return <b>true</b> if passed String is in upper case, otherwise <b>false</b>
     */
    static boolean isUpperCase(String str) {
        //97 - 122(a-z)  65 - 90(A-Z)
        //1072 - 1103(а-я)  1040 - 1071(А-Я)
        if(isNOE(str)) {
            return false;
        }
        char c;
        for(int i = 0; i < str.length(); i++) {
            c = str.charAt(i);
            if((c > 96 && c < 123) || (c > 1071 && c < 1104)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Is capitalized String (English lang)
     * @param s String to check
     * @return true if string is capitalized
     */
    static boolean isCapitalized(String s) {
        //97 - 122(a-z)  65 - 90(A-Z)
        //1072 - 1103(а-я)  1040 - 1071(А-Я)
        if(isNOE(s)) {
            return false;
        }
        char[] arr = s.toCharArray();
        char c;
        if((arr[0] > 96 && arr[0] < 123) || (arr[0] > 1071 && arr[0] < 1104)) {
            return false;
        }

        for(int i = 1; i < arr.length; i++) {
            c = arr[i];
            if((c > 64 && c < 91) || (c > 1039 && c < 1072)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Capitalize String (English lang)
     * @param s String to capitalize
     * @return capitalized String
     */
    static String capitalize(String s) {
        //97 - 122(a-z)  65 - 90(A-Z)
        //1072 - 1103(а-я)  1040 - 1071(А-Я)
        if(isNOE(s)) {
            return s;
        }
        char[] arr = s.toCharArray();
        char c;
        boolean isCap = true;

        if((arr[0] > 96 && arr[0] < 123) || (arr[0] > 1071 && arr[0] < 1104)) {
            arr[0] -= 32;
            isCap = false;
        }

        for(int i = 1; i < arr.length; i++) {
            c = arr[i];
            if((c > 64 && c < 91) || (c > 1039 && c < 1072)) {
                arr[i] += 32;
                isCap = false;
            }
        }
        return isCap ? s : new String(arr);//prevent creating new Object
    }

    /**
     * ...
     * @param s string to split
     * @param c separator
     * @return array of splitted strings
     */
    static String[] split(String s, char c) {
        if(isNOE(s)) {
            return new String[0];
        }
        int j = 0;
        ArrayList<String> result = new ArrayList<String>();
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == c) {
                result.add(s.substring(j, i));
                j = i+1;
            }
        }
        if(j < s.length()) {
            result.add(s.substring(j, s.length()));
        }

        return result.toArray(new String[result.size()]);
    }

    static String trim(String str) {
        return str == null ? null : str.trim();
    }

    static String trimToSize(String str, int size) {
        if(str == null || str.length() < size) {
            return str;
        }
        return str.substring(0, size);
    }

    static String toUpper(String str) {
        return str == null ? null : str.toUpperCase();
    }

    static String toLower(String str) {
        return str == null ? null : str.toLowerCase();
    }

    static String toHex(String str) {
        if(isNOE(str)) {
            return str;
        }

        try {
            return javax.xml.bind.DatatypeConverter.printHexBinary(str.getBytes("UTF-8"));
        } catch(UnsupportedEncodingException e) {
            return null;
        }
    }

    static String fromHex(String str) {
        if(isNOE(str)) {
            return str;
        }

        try {
            return new String(javax.xml.bind.DatatypeConverter.parseHexBinary(str), "UTF-8");
        } catch(UnsupportedEncodingException e) {
            return null;
        }
    }
}