package com.tosh.data;

import java.lang.ref.SoftReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * User: arsentyev
 * Date: 30.07.12
 */
public class SoftArrayList<T> extends AbstractList<T> {
    private final ArrayList<SoftReference<T>> list = new ArrayList<SoftReference<T>>();

    public int size() {
        return list.size();
    }

    public boolean add(T value) {
        if(value == null) {
            return false;
        }
        return list.add(wrap(value));
    }

    public boolean addAll(Collection<? extends T> collection) {
        if(collection == null || collection.isEmpty()) {
            return false;
        }
        for(T t: collection) {
            add(t);
        }
        return true;
    }

    public boolean addAll(int index, Collection<? extends T> collection) {
        if(collection == null || collection.isEmpty()) {
            return false;
        }
        for(T t: collection) {
            add(index++, t);
        }
        return true;
    }

    public void add(int index, T value) {
        if(value == null) {
            return;
        }
        list.add(index, wrap(value));
    }

    public T set(int index, T value) {
        if(value == null) {
            return null;
        }
        return unwrap(list.set(index, wrap(value)));
    }

    public T get(int index) {
        return unwrap(list.get(index));
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Iterator<SoftReference<T>> iterator = list.iterator();

            public boolean hasNext() {
                return iterator.hasNext();
            }

            public T next() {
               return unwrap(iterator.next());
            }

            public void remove() {
                iterator.remove();
            }
        };
    }

    public ListIterator<T> listIterator() {
        return new ListItr<T>(list.listIterator());
    }

    public ListIterator<T> listIterator(final int index) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }

        return new ListItr<T>(list.listIterator(index));
    }

    public boolean contains(Object obj) {
        if(obj == null) {
            return false;
        }

        for (SoftReference<T> aList : list) {
            if (obj.equals(unwrap(aList))) {
                return true;
            }
        }
        return false;
    }

    public boolean containsAll(Collection<?> collection) {
        if(collection == null || collection.isEmpty()) {
            return false;
        }

        for (Object obj: collection) {
            if (!contains(obj)) {
                return false;
            }
        }
        return true;
    }

    public int indexOf(Object obj) {
        if(obj == null) {
            return -1;
        }

        for(int i = 0; i < size(); i++) {
            if(obj.equals(get(i))) {
                return i;
            }
        }

        return -1;
    }

    public int lastIndexOf(Object obj) {
        if(obj == null) {
            return -1;
        }

        for(int i = size() - 1; i >= 0; i--) {
            if(obj.equals(get(i))) {
                return i;
            }
        }

        return -1;
    }

    public Object[] toArray() {
        Object[] objects = list.toArray();

        for (int i = 0; i < objects.length; i++) {
            objects[i] = unwrap((SoftReference<T>) objects[i]);
        }
        return objects;
    }

    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    public T remove(int index) {
        return unwrap(list.remove(index));
    }

    public boolean remove(Object obj) {
        if (obj == null) {
            return false;
        }
        Iterator<T> e = iterator();
        while (e.hasNext()) {
            if (obj.equals(e.next())) {
                e.remove();
                return true;
            }
        }
        return false;
    }

    public boolean removeAll(Collection<?> collection) {
        if(collection == null || collection.isEmpty()) {
            return false;
        }
        boolean modified = false;

        for(Object obj: collection) {
            remove(obj);
            modified = true;
        }
        return modified;
    }

    public void removeNullElements() {
        Iterator<T> e = iterator();
        while (e.hasNext()) {
            if (e.next() == null) {
                e.remove();
            }
        }
    }

    public List<T> subList(int fromIndex, int toIndex, boolean notNull) {
        throw new UnsupportedOperationException();
    }

    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection<?> collection) {
        boolean modified = false;
        Iterator<T> e = iterator();
        while (e.hasNext()) {
            if (!collection.contains(e.next())) {
                e.remove();
                modified = true;
            }
        }
        return modified;
    }

    private final class ListItr<T> implements ListIterator<T> {
        private final ListIterator<SoftReference<T>> iterator;

        ListItr(ListIterator<SoftReference<T>> iterator) {
            this.iterator = iterator;
        }

        public boolean hasNext() {
            return iterator.hasNext();
        }

        public T next() {
            return unwrap(iterator.next());
        }

        public boolean hasPrevious() {
            return iterator.hasPrevious();
        }

        public T previous() {
            return unwrap(iterator.previous());
        }

        public int nextIndex() {
            return iterator.nextIndex();
        }

        public int previousIndex() {
            return iterator.previousIndex();
        }

        public void remove() {
            iterator.remove();
        }

        public void set(T t) {
            if (t == null) {
                return;
            }
            iterator.set(wrap(t));
        }

        public void add(T t) {
            if (t == null) {
                return;
            }
            iterator.add(wrap(t));
        }
    }

    private <T> T unwrap(SoftReference<T> value) {
        return value == null ? null : value.get();
    }

    private <T> SoftReference<T> wrap(T value) {
        return new SoftReference<T>(value);
    }
}