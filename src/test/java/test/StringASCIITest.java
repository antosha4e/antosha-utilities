package test;

import com.tosh.data.StringASCII;
import junit.framework.TestCase;

/**
 * User: arsentyev
 * Date: 25.10.12
 */
public class StringASCIITest extends TestCase {
    public void testString() throws Exception {
        StringASCII str = new StringASCII("abc");

        assertEquals('a', str.charAt(0));
        assertEquals("abc", str.toString());

        str = new StringASCII("abcdef");
        assertEquals("def", str.subSequence(3, 6).toString());

        assertTrue(str.startsWith("bc", 1));
        assertTrue(str.endsWith("def"));
    }
}