package com.tosh.iterate;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Facade enumeration as Iterator
 *
 * User: arsentyev
 * Date: 14.01.13
 */
public class EnumerationIterator<T> implements Iterator<T>, Iterable<T> {
    private Enumeration<T> enumeration;

    public EnumerationIterator(Enumeration<T> enumeration) {
        this.enumeration = enumeration;
    }

    @Override
    public boolean hasNext() {
        return enumeration.hasMoreElements();
    }

    @Override
    public T next() {
        return enumeration.nextElement();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }
}