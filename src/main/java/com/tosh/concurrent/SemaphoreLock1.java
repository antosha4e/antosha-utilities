package com.tosh.concurrent;

import java.util.concurrent.Semaphore;

/**
 * User: arsentyev
 * Date: 16.08.12
 */
public class SemaphoreLock1 {
    public SemaphoreLock1() {
        readers = 0;
        readLock = new Semaphore(1);
        writeLock = new Semaphore(1);
    }

    public void writeLock() throws InterruptedException {
        writeLock.acquire();
    }

    public void writeUnlock() throws InterruptedException {
        writeLock.release();
    }

    public void readLock() throws InterruptedException {
        readLock.acquire();
        if(readers == 0) {
            writeLock.acquire();
        }
        readers++;
        readLock.release();
    }

    public void readUnlock() throws InterruptedException {
//        assert readers > 0;
        if(readers == 0) {
            return;
        }
        readLock.acquire();
        readers--;
        if(readers == 0) {
            writeLock.release();
        }
        readLock.release();
    }

    private final Semaphore readLock;
    private final Semaphore writeLock;
    private int readers;
}