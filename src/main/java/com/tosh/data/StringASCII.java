package com.tosh.data;

import java.io.UnsupportedEncodingException;

/**
 * Representing String with ASCII symbols
 *
 * <br>
 * User: arsentyev
 * Date: 25.09.12
 */
public class StringASCII implements CharSequence {
    private byte[] bytes;

    /** Cache the hash code for the string */
    private int hash; // Default to 0

    private StringASCII() {}

    public StringASCII(char[] chars) {
        if(chars == null) {
            return;
        }
        bytes = new byte[chars.length];
        for(int i = 0; i < chars.length; i++) {
            bytes[i] = (byte) chars[i];
        }
    }

    private StringASCII(byte [] bytes1) {
        if(bytes1 == null) {
            return;
        }
        bytes = bytes1;
    }

    public StringASCII(String str) {
        if(str == null || str.isEmpty()) {
            return;
        }
        try {
            bytes = str.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {}
    }

    public String getString() {
        try {
            return new String(bytes, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

//    @Override
    public boolean equals1(Object obj){
        if(obj == null) {
            return false;
        }
        if(obj.getClass() == String.class) {
            return getString().equals(obj);
        }
        return obj.getClass() == StringASCII.class && ((StringASCII)obj).getString().equals(getString());
    }

    public boolean equals(Object anObject) {
        if (this == anObject) {
            return true;
        }
        if (anObject.getClass() == String.class) {
            String anotherString = (String) anObject;
            int n = length();
            if (n == anotherString.length()) {
                for (int i = 0; i < n; i++) {
                    if ((char) bytes[i] != anotherString.charAt(i))
                        return false;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a hash code for this string. The hash code for a
     * <code>String</code> object is computed as
     * <blockquote><pre>
     * s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
     * </pre></blockquote>
     * using <code>int</code> arithmetic, where <code>s[i]</code> is the
     * <i>i</i>th character of the string, <code>n</code> is the length of
     * the string, and <code>^</code> indicates exponentiation.
     * (The hash value of the empty string is zero.)
     *
     * @return  a hash code value for this object.
     */
    public int hashCode() {
        int h = hash;
        int len = bytes.length;
        if (h == 0 && len > 0) {
            byte val[] = bytes;

            for (int i = 0; i < len; i++) {
                h = 31 * h + val[i];
            }
            hash = h;
        }
        return h;
    }

    @Override
    public int length() {
        return bytes.length;
    }

    @Override
    public char charAt(int index) {
        if(index < 0 || index >= length()) {
            throw new IndexOutOfBoundsException("");
        }
        return (char)bytes[index];
    }

    /**
     * Returns a new <code>CharSequence</code> that is a subsequence of this sequence.
     * The subsequence starts with the <code>char</code> value at the specified index and
     * ends with the <code>char</code> value at index <tt>end - 1</tt>.  The length
     * (in <code>char</code>s) of the
     * returned sequence is <tt>end - start</tt>, so if <tt>start == end</tt>
     * then an empty sequence is returned. </p>
     *
     * @param   start   the start index, inclusive
     * @param   end     the end index, exclusive
     *
     * @return  the specified subsequence
     *
     * @throws  IndexOutOfBoundsException
     *          if <tt>start</tt> or <tt>end</tt> are negative,
     *          if <tt>end</tt> is greater than <tt>length()</tt>,
     *          or if <tt>start</tt> is greater than <tt>end</tt>
     */
    @Override
    public CharSequence subSequence(int start, int end) {
        if(end > length() || start > end || start < 0 || end < 0) {
            throw new IndexOutOfBoundsException("");
        }
        byte[] bytes1 = new byte[end - start];
        System.arraycopy(bytes, start, bytes1, 0, bytes1.length);
        return new StringASCII(bytes1);
    }

    @Override
    public String toString() {
        return getString();
    }

    /**
     * Returns <tt>true</tt> if, and only if, {@link #length()} is <tt>0</tt>.
     *
     * @return <tt>true</tt> if {@link #length()} is <tt>0</tt>, otherwise
     * <tt>false</tt>
     *
     * @since 1.6
     */
    public boolean isEmpty() {
        return length() == 0;
    }

    public boolean startsWith(String prefix, int toffset) {
        int pl = prefix.length();

        if ((toffset < 0) || (toffset > length() - pl)) {
            return false;
        }
        for (int i = 0; i < pl; i++) {
            if ((char) bytes[toffset + i] != prefix.charAt(i))
                return false;
        }
        return true;
    }

    public boolean startsWith(String prefix) {
        return startsWith(prefix, 0);
    }

    public boolean endsWith(String suffix) {
        return startsWith(suffix, length() - suffix.length());
    }
}