package test;

import com.tosh.log.ToshLogger;
import junit.framework.TestCase;

/**
 * User: arsentyev
 * Date: 02.11.12
 */
public class LogTest extends TestCase {
    public void testLog() {
        new Log2().log();
    }

    private static class Log2 extends Log1 {
        public void log() {
            logme("OMG");
        }
    }

    private static class Log1 {
        protected void logme(String msg) {
            ToshLogger.getLogger(this.getClass()).severe("OMG");
        }
    }
}