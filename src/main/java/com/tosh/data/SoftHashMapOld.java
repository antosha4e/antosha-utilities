package com.tosh.data;

import java.io.Serializable;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * User: arsentyev
 * Date: 20.07.12
 */
public class SoftHashMapOld<K, V> extends AbstractMap<K, V> implements Serializable {
    private final Map<K, SoftValue<K, V>> hash = new HashMap<K, SoftValue<K, V>>();
    private final ReferenceQueue<V> queue = new ReferenceQueue<V>();

    public V get(Object key) {
        V result = unwrap(hash.get(key));
        if (result == null) {
            hash.remove(key);//mb remove this stuff
        }
        return result;
    }

    public V put(K key, V value) {
        processQueue();
        return unwrap(hash.put(key, new SoftValue<K, V>(key, value, queue)));
    }

    public V remove(Object key) {
        processQueue();
        return unwrap(hash.remove(key));
    }

    public void clear() {
        processQueue();
        hash.clear();
    }

    public int size() {
        processQueue();
        return hash.size();
    }

    public Set<Entry<K, V>> entrySet() {
        processQueue();
        Set<Entry<K, V>> result = new LinkedHashSet<Entry<K, V>>();
        for (final Entry<K, SoftValue<K, V>> entry : hash.entrySet()) {
            final V value = unwrap(entry.getValue());
            if (value != null) {
                result.add(new Entry<K, V>() {
                    public K getKey() {
                        return entry.getKey();
                    }

                    public V getValue() {
                        return value;
                    }

                    public V setValue(V v) {
                        entry.setValue(new SoftValue<K, V>(getKey(), v, queue));
                        return value;
                    }
                });
            }
        }
        return result;
    }

    private V unwrap(SoftValue<K, V> value) {
        return value == null ? null : value.get();
    }

    private static class SoftValue<K, V> extends SoftReference<V> {
        private final K key;
        private final V value;

        private SoftValue(K key, V value, ReferenceQueue<V> q) {
            super(value, q);
            this.key = key;
            this.value = value;
        }
    }

    private void processQueue() {
        SoftValue sv;
        while ((sv = (SoftValue) queue.poll()) != null) {
            hash.remove(sv.key);
        }
    }
}