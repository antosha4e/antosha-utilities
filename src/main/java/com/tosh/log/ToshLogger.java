package com.tosh.log;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class ToshLogger {
    private final Logger logger;

    private ToshLogger(String name) {
        this.logger = name == null ? Logger.getAnonymousLogger() : Logger.getLogger(name);
    }

    public static ToshLogger getLogger(String name) {
        return new ToshLogger(name);
    }

    public static ToshLogger getLogger(Class clazz) {
        return new ToshLogger(clazz.getName());
    }

    public static ToshLogger getLogger(Object obj) {
        if(obj == null) {
            return getAnonymousLogger();
        }
        return new ToshLogger(obj.getClass().getName());
    }

    public static ToshLogger getAnonymousLogger() {
        return new ToshLogger(null);
    }

    public void addHandler(Handler handler) throws SecurityException {
        this.logger.addHandler(handler);
    }

    public void removeHandler(Handler handler) throws SecurityException {
        this.logger.removeHandler(handler);
    }

    public void log(LogRecord record) {
        this.logger.log(record);
    }

    public void log(Level level, String msg) {
        LogRecord lr = new LogRecord(level, msg);
        lr.setSourceClassName(this.logger.getName());
        lr.setSourceMethodName(getMethodName());
        this.log(lr);
    }

    public void log(Level level, String msg, String methodName) {
        LogRecord lr = new LogRecord(level, msg);
        lr.setSourceClassName(this.logger.getName());
        lr.setSourceMethodName(methodName);
        this.log(lr);
    }

    public void log(Level level, String msg, Object param1) {
        LogRecord lr = new LogRecord(level, msg);
        Object params[] = { param1 };
        lr.setParameters(params);
        lr.setSourceClassName(this.logger.getName());
        lr.setSourceMethodName(getMethodName());
        this.log(lr);
    }

    public void log(Level level, String msg, Object... params) {
        LogRecord lr = new LogRecord(level, msg);
        lr.setParameters(params);
        lr.setSourceClassName(this.logger.getName());
        lr.setSourceMethodName(getMethodName());
        this.log(lr);
    }

    public void log(Level level, String msg, Throwable thrown) {
        LogRecord lr = new LogRecord(level, msg);
        lr.setThrown(thrown);
        lr.setSourceClassName(this.logger.getName());
        lr.setSourceMethodName(getMethodName());
        this.log(lr);
    }

    public void severe(String msg) {
        log(Level.SEVERE, msg, getMethodName());
    }

    public void warning(String msg) {
        log(Level.WARNING, msg, getMethodName());
    }

    public void info(String msg) {
        log(Level.INFO, msg, getMethodName());
    }

    public void config(String msg) {
        log(Level.CONFIG, msg, getMethodName());
    }

    public void fine(String msg) {
        log(Level.FINE, msg, getMethodName());
    }

    private String getMethodName() {
        String method = "";
        String className = null;
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        for(StackTraceElement element : elements) {
            className = element.getClassName();
            if(className.equals(this.logger.getName())) {
                return element.getMethodName();
            }
        }
        return method;
    }
}