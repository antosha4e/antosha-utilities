package com.tosh.concurrent;

import java.util.concurrent.Semaphore;

/**
 * User: arsentyev
 * Date: 16.08.12
 */
public class SemaphoreLock2 extends Semaphore {
    private final int nThreads;

    public SemaphoreLock2() {
        this(10);
    }

    public SemaphoreLock2(int permits) {
        super(permits, true);
        nThreads = permits;
    }

    public void writeLock() throws InterruptedException {
        acquire(nThreads);
    }

    public void writeUnlock() throws InterruptedException {
        release(nThreads);
    }

    public void readLock() throws InterruptedException {
        acquire();
    }

    public void readUnlock() throws InterruptedException {
        release();
    }
}