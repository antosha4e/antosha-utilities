package com.tosh.sql;

/**
 * User: antosha
 * Date: 24.12.13
 */
public enum OrderBy {
    ASC, DESC
}