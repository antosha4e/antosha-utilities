package com.tosh.sql;

/**
 * Not finished yet...
 *
 * User: arsentyev
 * Date: 30.11.12
 */
class SQLQuery {
    private StringBuilder query = new StringBuilder();

    //indexes of main points
    private int fromInd = 0;
    private int joinInd = 0;
    private int whereInd = 0;
    private int orderInd = 0;
    private int groupInd = 0;

    public SQLQuery select(String... columns) {
        query.append("select ");
        if(columns == null || columns.length == 0) {
            query.append("* ");
        } else {
            for(String column: columns) {
                query.append(column).append(",");
            }
            query.deleteCharAt(query.length()-1);
        }
        //fromInd = query.length() - 1;
        return this;
    }

    public SQLQuery update(String table) {
        query.append("update ").append(table);
        return this;
    }

    public SQLQuery delete(String table) {
        query.append("delete from ").append(table);
        return this;
    }

    public SQLQuery insert(String table) {
        query.append("insert into ").append(table);
        return this;
    }

    public SQLQuery values(String... values) {
        query.append("values(");
        for(String value: values) {
            query.append(value).append(",");
        }
        query.deleteCharAt(query.length()-1).append(")");
        return this;
    }

    public SQLQuery from(String table) {
        query.append(" from ").append(table);
        return this;
    }

    public SQLQuery from(String table, String alias) {
        query.append(" from ").append(table).append(" as ").append(alias);
        return this;
    }

    public SQLQuery on(String onClause) {
        query.append(" on ").append(onClause);
        return this;
    }

    public SQLQuery join(String table) {
        return join(table, JoinType.INNER);
    }

    public SQLQuery join(String table, String alias) {
        return join(table, alias, JoinType.INNER);
    }

    public SQLQuery join(String table, JoinType type) {
        query.append(" ").append(type.name().toLowerCase()).append(" join ").append(table);
        return this;
    }

    public SQLQuery join(String table, String alias, JoinType type) {
        query.append(" ").append(type.name().toLowerCase()).append(" join ").append(table).append(" as ").append(alias);
        return this;
    }

    public SQLQuery set(String clause) {
        query.append(" set ").append(clause);
        return this;
    }

    public SQLQuery where(String clause) {
        query.append(" where ").append(clause);
        return this;
    }

    public SQLQuery orderBy(String column) {
        return orderBy(column, OrderType.ASC);
    }

    public SQLQuery orderBy(String column, boolean asc) {
        return orderBy(column, asc? OrderType.ASC : OrderType.DESC);
    }

    public SQLQuery orderBy(String column, OrderType type) {
        query.append(" order by ").append(column).append(" ").append(type.name().toLowerCase());
        return this;
    }

    public SQLQuery groupBy(String column) {
        query.append(" group by ").append(column);
        return this;
    }

    public SQLQuery having(String havingClause) {
        query.append(" having ").append(havingClause);
        return this;
    }

    public String toString() {
        return query.toString();
    }

    public void reset() {
        query.setLength(0);
    }

    public static enum JoinType {
        INNER, FULL, LEFT, RIGHT
    }

    public static enum OrderType {
        ASC, DESC
    }

    public static void main(String... args) {
        SQLQuery query = new SQLQuery();
        query.select("u.id", "u.name", "a.id")
             .from("users", "u")
             .join("accounts", "a").on("a.id = u.accId")
             .where("u.name like '%tosha%'")
             .orderBy("u.name", OrderType.ASC);

        System.out.println(query);

        query.reset();

        query.update("accounts")
             .set("amount = 1000")
             .where("id > 1000");

        System.out.println(query);
    }
}